let params = new URLSearchParams(window.location.search);
let userId = params.get('userId');
let firstName = document.querySelector("#firstName");
let lastName = document.querySelector("#lastName");
let mobileNo = document.querySelector("#mobileNo");
let token = localStorage.getItem('token');

fetch(`https://secret-tor-47846.herokuapp.com/api/users/${userId}`)
.then(res => res.json())
.then(data => {

	firstName.placeholder = data.firstName
	lastName.placeholder = data.lastName
    mobileNo.placeholder = data.mobileNo
	firstName.value = data.firstName
    lastName.value = data.lastName
    mobileNo.value = data.mobileNo

})

document.querySelector("#editProfile").addEventListener("submit", (e) => {

	e.preventDefault()


	let fName = firstName.value
    let lName = lastName.value
    let mNo = mobileNo.value
    // console.log(fName)
    // console.log(lName)
    // console.log(mNo)


	let token = localStorage.getItem('token')

	fetch('https://secret-tor-47846.herokuapp.com/api/users/update', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            firstName: fName,
            lastName: lName,
            mobileNo: mNo,
        })
    })
    .then(res => res.json())
    .then(data => {
        
    	// console.log(data)

    	//creation of new course successful
    	if(data === true){
    	    //redirect to courses index page
    	    window.location.replace("./profile.html")
    	}else{
    	    //error in creating course, redirect to error page
    	    alert("something went wrong")
    	}

    })

})