let adminCheck = localStorage.getItem('isAdmin') === "true"
let profile = document.querySelector('#profile');
let bookNow = document.querySelector('#bookNow');
let tokenCheck = localStorage.getItem('token');

if (!adminCheck && tokenCheck != null){
	profile.innerHTML =
	`
		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>
	`
	bookNow.innerHTML =
	`
		<a href="./courses.html" class="btn btn-outline-primary">Book Now</a>
	`
} else {
	profile.innerHTML = null
	bookNow.innerHTML = null
}