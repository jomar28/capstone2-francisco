let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin") === "true"
let addButton = document.querySelector("#adminButton")

if (adminUser === false || adminUser === null) {
	addButton.innerHTML = null
} else {
	addButton.innerHTML = `

	<div class="col-md-2 offset-md-10">
		<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
	</div>

	`

}

fetch('https://secret-tor-47846.herokuapp.com/api/courses/')
.then(res => res.json())
.then(data => {
	console.log(data)
})