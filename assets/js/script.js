let navItems = document.querySelector("#navSession");
let userToken = localStorage.getItem("token");
let registerLink = document.querySelector("#register");

if (!userToken) {
	navItems.innerHTML =
	`
		<li class="nav-item ">
			<a href="./login.html" class="nav-link"> Login </a>
		</li>
	`
	registerLink.innerHTML =
	`
		<li class="nav-item ">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>
	`
} else {
	navItems.innerHTML =
	`
		<li class="nav-item ">
			<a href="./logout.html" class="nav-link"> Log Out </a>
		</li>
	`
}