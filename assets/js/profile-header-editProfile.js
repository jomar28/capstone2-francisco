let adminCheck = localStorage.getItem('isAdmin') === "true"
let profile = document.querySelector('#profile')
let tokenCheck = localStorage.getItem('token');


if (!adminCheck && tokenCheck != null){
	profile.innerHTML =
	`
		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>
	`
} else {
	profile.innerHTML = null
}