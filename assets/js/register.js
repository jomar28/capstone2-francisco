
let registerForm = document.querySelector("#registerUser");

// let firstName = document.querySelector("#firsName");
// let lastName = document.querySelector("#lastName");
// let mobileNo = document.querySelector("#mobileNumber");
// let email = document.querySelector("#userEmail");
// let password1 = document.querySelector("#password1");
// let password2 = document.querySelector("#password2");

// console.log(firstName, lastName, mobileNo, email, password1, password2);

registerForm.addEventListener("submit", (e) => { //e = event object. This event object pertains to the event and where it was triggered.
	e.preventDefault() //this method prevents default behavior of submit event
	console.log("I triggered the submit event")
	let firstName = document.querySelector("#firsName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

	if((password1 !== "" && password2 !== "" ) && (password1 === password2) && (mobileNo.length === 11)) {
		fetch('https://secret-tor-47846.herokuapp.com/api/users/email-exists', {
			method: "POST",
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === false) {

				fetch('https://secret-tor-47846.herokuapp.com/api/users', {
					method: "POST",
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data === true) {
						alert("registered successfully")
						window.location.replace("./login.html")
					} else {
						alert("something went wrong")
					}
				})
			}
		})
	}
})
