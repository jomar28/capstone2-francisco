let name = document.querySelector("#courseName");
let price = document.querySelector("#coursePrice");
let desc = document.querySelector("#courseDesc");
let courseEnrollees = document.querySelector("#enrollContainer");
let courseEditSubmit = document.querySelector("#courseEditSubmit");
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let jwtToken = localStorage.getItem('token');

// console.log(name,desc,userCourses);

fetch(`https://secret-tor-47846.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	if (data) {
		console.log(data)
		name.innerHTML =` ${data.name}`
		price.innerHTML =` ${data.price}`
		desc.innerHTML =` ${data.description}`
		courseEditSubmit.innerHTML =
		`
		<a href="./editCourse.html?courseId=${data._id}" value={data._id} class="btn text-white btn-block editButton my-5" style="background-color: #800000!important;">
		Edit
		</a>
		`

	} else {

		alert("Something went wrong");
	}
	if (data.enrollees.length === 0) {
				courseEnrollees.innerHTML = `<h5 class="card-title my-5">No enrollees</h5>`
			}	


	// data.enrollments.forEach(course => {
	// 	console.log(course)
	// })  

	

	data.enrollees.forEach(user => {
	// for (let i = 0; i < data.enrollments.length; i++) {

		let userId = user.userId

		

		fetch(`https://secret-tor-47846.herokuapp.com/api/users/${userId}`)
		// 	, {

		// 	headers: {
		// 		'Authorization': `Bearer ${jwtToken}`
		// 	}			
		// })
		.then(res => res.json())
		.then(data =>{
			if (data) {

				// console.log(data.enrollees)
				courseEnrollees.innerHTML +=
				` 
					<div class="card my-4">
						<div class="card-body">
							<h5 class="card-title">${data.firstName} ${data.lastName}</h5>
						</div>
					</div>
				`
			} else {
				alert("Something went wrong");
			}	
		})
	})	
})