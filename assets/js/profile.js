// console.log('hi from profile')
let name = document.querySelector("#userName");
let desc = document.querySelector("#userDesc");
let userCourses = document.querySelector("#enrollContainer");
let token = localStorage.getItem('token');
let updateProfile = document.querySelector("#updateProfile");

// console.log(name,desc,userCourses);

fetch(`https://secret-tor-47846.herokuapp.com/api/users/details`,{

	headers: {
		'Authorization' : `Bearer ${token}`
	}
	
})
.then(res => res.json())
.then(data =>{

	// console.log(data)
	// console.log(data.enrollments[0].courseId)
	// console.log(data.enrollments.length)
	if (data.enrollments.length === 0) {
				userCourses.innerHTML = `<h5 class="card-title my-5">Not enrolled to any course</h5>`
			}	
	if (data) {

		name.innerHTML =` ${data.firstName}  ${data.lastName}`
		desc.innerHTML =
		`
			<div>
				Mobile Number: ${data.mobileNo}
			</div>
			<div>
				Email: ${data.email}
			</div>  
		`
		updateProfile.innerHTML = 
		`
		<div>
			<a href="./editProfile.html?userId=${data._id}" value={data._id} class="btn text-white btn-block editButton my-5" style="background-color: #800000!important;">
				Update Profile
			</a>
		</div>
		`
	} else {

		alert("Something went wrong");
	}


	// data.enrollments.forEach(course => {
	// 	console.log(course)
	// })  

	

	data.enrollments.forEach(course => {
	// for (let i = 0; i < data.enrollments.length; i++) {

		let courseId = course.courseId


		fetch(`https://secret-tor-47846.herokuapp.com/api/courses/${courseId}`,{

			headers: {
				// "Content-Type": 'application/json',
				'Authorization': `Bearer ${token}`
			}
			// body: JSON.stringify({
			// 	_id: data.enrollments[i].courseId
			// })
			
		})
		.then(res => res.json())
		.then(data =>{		
			// console.log(data.name)	
		
			if (data) {
				userCourses.innerHTML +=
				` 
					<div class="card my-4">
						<div class="card-body">
							<h5 class="card-title">${data.name}</h5>
						</div>
					</div>
				`
				// courseArray.push(data.name)
				// console.log(courseArray)
			} else {
				alert("Something went wrong");
			}

			// courseArray.forEach(courseName => console.log(courseName));
		})

	})	

})