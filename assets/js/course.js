let params = new URLSearchParams(window.location.search)
// console.log(params.has('courseId'))
// console.log(params.get('courseId'))
let courseId = params.get('courseId')

let token = localStorage.getItem('token')

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

fetch(`https://secret-tor-47846.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block text-white" style="background-color: #800000!important;">Enroll</button>`

	document.querySelector('#enrollButton').addEventListener("click", () => {
		fetch('https://secret-tor-47846.herokuapp.com/api/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data === true) {

				alert('Thank you for enrolling to the course.');
				window.location.replace('./courses.html');
			} else {

				alert('Register first')
				window.location.replace('./register.html');
			}
		})
		.catch((err) => {
            alert("You are already enrolled to this Course");
            window.location.replace('./courses.html');
          });
	})
})