let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin") === "true"
let addButton = document.querySelector("#adminButton")
let cardFooter;

//This if statement will allow us to show a button for adding a course;a button to redirect us to the addCourse page if the user is admin, however, if he/she is a guest or a regular user, they should not see a button.
if(adminUser === false || adminUser === null){

	addButton.innerHTML = null

} else {
	addButton.innerHTML = `

		<div class="col-md-3 offset-md-9">
			<a href="./addCourse.html" class="btn btn-block text-white my-3" style="background-color: #800000!important; margin-top: -15px!important;">Add Course</a>
		</div>


	`
}

fetch('https://secret-tor-47846.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {
	console.log(data);

	let courseData;

	if (data.length < 1) {
		courseData = "No courses available";
	} else {
		courseData = data.map(course => {
			console.log(course)
			if(adminUser == false || !adminUser) {
				if(course.isActive === true){
					console.log(course);
				cardFooter =
				`
				<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-dark text-white btn-block editButton" style="background-color: #800000!important;">Select Course</a>
				`
				return(
				`
					<div class="col-md-4 my-3">
						<div class="card h-100">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									₱ ${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>

				`
			)






				}
			} else {

				if (course.isActive === true){
				cardFooter =

					 `
						
					 <a href="./disableCourse.html?courseId=${course._id}" value={course._id}" class="btn  text-white btn-block disableButton" style="background-color: #FECB00!important;"> 
					 Disable
					 </a>
					 <a href="./courseDetails.html?courseId=${course._id}" value={course._id}" class="btn text-white btn-block viewButton" style="background-color: #800000!important;"> 
					 View Course
					 </a>
						

					`
				}else {
				cardFooter =

						`
						<a href="./enableCourse.html?courseId=${course._id}" value={course._id}" class="btn text-white btn-block enableButton" style="background-color: #800000!important;"> 
						Enable
						</a>
						<a href="./courseDetails.html?courseId=${course._id}" value={course._id}" class="btn text-white btn-block viewButton" style="background-color: #800000!important;"> 
						View Course
						</a>

						`



				}
				return(
			
				`
					<div class="col-md-6 my-3">
						<div class="card h-100">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									₱ ${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`
			)
			}


			
				//since the collection is an array , we can use the join method to indicate the separator for each element
		}).join("")
	}

	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData;	
})	

