let params = new URLSearchParams(window.location.search)
let jwtToken = localStorage.getItem('token')
let courseId = params.get('courseId')

fetch(`https://secret-tor-47846.herokuapp.com/api/courses/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${jwtToken}`
	},
	body: JSON.stringify({
		courseId: courseId
	})
})
.then(res => res.json())
.then(data =>{
	console.log(data)
	if (data === true){
		window.location.replace('./courses.html')
	} else {
		alert("Something went wrong.")
	}
})