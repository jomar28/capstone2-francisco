let navItems = document.querySelector("#navSession");
let userToken = localStorage.getItem("token");
let registerLink = document.querySelector("#register");

if (!userToken) {
	navItems.innerHTML =
	`
		<li class="nav-item ">
			<a href="./pages/login.html" class="nav-link"> Login </a>
		</li>
	`
	register.innerHTML =
	`
		<li class="nav-item ">
			<a href="./pages/register.html" class="nav-link"> Register </a>
		</li>
	`
} else {
	navItems.innerHTML =
	`
		<li class="nav-item ">
			<a href="./pages/logout.html" class="nav-link"> Log Out </a>
		</li>
	`
}